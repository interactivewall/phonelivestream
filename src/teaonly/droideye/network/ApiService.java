package teaonly.droideye.network;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.PUT;

public interface ApiService {

	@PUT("/telemetry")
	Call<Object> telemetry(@Body TelemetryRequest request);
}
