package teaonly.droideye.network;

public class TelemetryRequest {

	public String clientId;
	public String clientType;
	public String sensorType;
	public Data data;
	
	public static class Data {
		
		public Position position;
		public float speed;
		
		public static class Position {
			public double latitude;
			public double longitude;
		}
	}
	
}
