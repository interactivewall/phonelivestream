package teaonly.droideye;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;

public class Config {
	
	public static String getTelemetryUrl(Context context) {
		String result = getSharedPreferences(context)
				.getString(
						context.getString(R.string.prefs_key_telemetry_url), 
						context.getString(R.string.prefs_def_telemetry_url));
		if(TextUtils.isEmpty(result)) {
			result = context.getString(R.string.prefs_def_telemetry_url);
		}
		if(result.charAt(result.length() - 1) != '/') {
			result += "/";
		}
		return result;
	}
	
	public static String getClientId(Context context) {
		return getSharedPreferences(context)
				.getString(
						context.getString(R.string.prefs_key_client_id), 
						context.getString(R.string.prefs_def_client_id));
	}
	
	public static String getClientType(Context context) {
		return getSharedPreferences(context)
				.getString(
						context.getString(R.string.prefs_key_client_type), 
						context.getString(R.string.prefs_def_client_type));
	}
	
	public static float getMinGPSDistance(Context context) {
		return Float.parseFloat(
				getSharedPreferences(context)
					.getString(
						context.getString(R.string.prefs_key_min_gps_distance), 
						context.getString(R.string.prefs_def_min_gps_distance)));
	}
	
	public static long getMinGPSTime(Context context) {
		return Long.parseLong(
				getSharedPreferences(context)
					.getString(
						context.getString(R.string.prefs_key_min_gps_time), 
						context.getString(R.string.prefs_def_min_gps_time)));
	}
	
	public static int getBufFramesCount(Context context) {
		return Integer.parseInt(getSharedPreferences(context)
					.getString(
						context.getString(R.string.prefs_key_buf_frames_count), 
						context.getString(R.string.prefs_def_buf_frames_count)));
	}
	
	private static SharedPreferences getSharedPreferences(Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context);
	}
	
}
