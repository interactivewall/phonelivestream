package teaonly.droideye;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import teaonly.droideye.network.ApiService;
import teaonly.droideye.network.TelemetryRequest;
import teaonly.droideye.network.TelemetryRequest.Data;
import teaonly.droideye.network.TelemetryRequest.Data.Position;
import android.app.Activity;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.google.gson.GsonBuilder;

public class MainActivity extends Activity implements /*CameraView.CameraReadyCallback,*/ LocationListener {
	
	private LocationManager locationManager;
	
    public static String TAG="TEAONLY";
    /*private final int ServerPort = 8080;
    private final int StreamingPort = 8088;
    private final int PictureWidth = 320;
    private final int PictureHeight = 240;
    private final int MediaBlockNumber = 3;
    private final int MediaBlockSize = 1024*512;
    private int EstimatedFrameNumber = 1;*/

    //private StreamingServer streamingServer = null;
    //private TeaServer webServer = null;
    //private OverlayView overlayView = null;
    //private CameraView cameraView = null;

    //ExecutorService executor = Executors.newFixedThreadPool(3);
    //VideoEncodingTask videoTask = new  VideoEncodingTask();
    //private ReentrantLock previewLock = new ReentrantLock();
    //boolean inProcessing = false;

    //byte[] yuvFrame = new byte[1920*1280*2];

    //MediaBlock[] mediaBlocks = new MediaBlock[MediaBlockNumber];
    //int mediaWriteIndex = 0;
    //int mediaReadIndex = 0;

    //Handler streamingHandler;

    private OkHttpClient okHttpClient;
    private ApiService apiService;
    
    private String currentIpAddress;

    //
    //  Activiity's event handler
    //
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	OkHttpClient.Builder builder = new OkHttpClient.Builder()
		.connectTimeout(1, TimeUnit.MINUTES)
		.writeTimeout(1, TimeUnit.MINUTES)
		.readTimeout(1, TimeUnit.MINUTES);
		if(BuildConfig.DEBUG) {
			try {
				HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
				interceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS);
				builder.addNetworkInterceptor(interceptor);
			} catch (Throwable t) {
				Log.e(TAG, "Failed to add HttpLoggingInterceptor", t);
			}
		}
		okHttpClient = builder.build();
    	
        // application setting
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window win = getWindow();
        win.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // load and setup GUI
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        findViewById(R.id.content).setOnLongClickListener(new View.OnLongClickListener() {
			
			@Override
			public boolean onLongClick(View v) {
				SettingsActivity.start(MainActivity.this, currentIpAddress);
				return false;
			}
		});
        // init audio and camera
        /*for(int i = 0; i < MediaBlockNumber; i++) {
            mediaBlocks[i] = new MediaBlock(MediaBlockSize);
        }
        resetMediaBuffer();

        try {
            streamingServer = new StreamingServer(StreamingPort);
            streamingServer.start();
        } catch (UnknownHostException e) {
            return;
        }

        if ( initWebServer() ) {
            initCamera();
        } else {
            return;
        }

        streamingHandler = new Handler();
        streamingHandler.post(new Runnable() {
            @Override
            public void run() {
                doStreaming();
            }
        });
        */

    }
    
    @Override
    protected void onPause() {
    	super.onPause();
    	if(locationManager != null) {
        	locationManager.removeUpdates(this);
        }
    }
    
    @Override
    protected void onResume() {
    	super.onResume();
    	//EstimatedFrameNumber = Config.getBufFramesCount(this);
    	Retrofit retrofit = new Retrofit.Builder()
			.baseUrl(Config.getTelemetryUrl(this))
			.client(okHttpClient)
			.addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setLenient().create()))
			.build();
    	apiService = retrofit.create(ApiService.class);
    	LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        locationManager.requestLocationUpdates(
        		LocationManager.GPS_PROVIDER, 
        		Config.getMinGPSTime(this), 
        		Config.getMinGPSDistance(this), 
        		this);
    }
    
    @Override
    public void onDestroy() {
    	/*if(streamingServer != null) {
        	try {
				streamingServer.stop();
			} catch (Exception e) {
				Log.e(TAG, "Failed to stop streaming server", e);
			}
        }
        if (webServer != null) {
            webServer.stop();
        }
        if (cameraView != null) {
            previewLock.lock();
            cameraView.StopPreview();
            cameraView.Release();
            previewLock.unlock();
            cameraView = null;
        }*/
        
        super.onDestroy();
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	getMenuInflater().inflate(R.menu.menu_main, menu);
    	return super.onCreateOptionsMenu(menu);
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
		case R.id.menu_settings:
			Intent i = new Intent(this, SettingsActivity.class);
			startActivity(i);
			return true;

		default:
			break;
		}
    	return super.onOptionsItemSelected(item);
    }
    */
    //
    //  Interface implementation
    //
    /*public void onCameraReady() {
    	if(cameraView != null) { // HOTFIX!
	        cameraView.StopPreview();
	        cameraView.setupCamera(PictureWidth, PictureHeight, 4, 25.0, previewCb);
	        nativeInitMediaEncoder(cameraView.Width(), cameraView.Height());
	        cameraView.StartPreview();
    	}
    }*/

    //
    //  Internal help functions
    //
    /*private boolean initWebServer() {

        String ipAddr = wifiIpAddress(this);
        if ( ipAddr != null ) {
            try{
                webServer = new TeaServer(8080, this);
                webServer.registerCGI("/cgi/query", doQuery);
            }catch (IOException e){
                webServer = null;
            }
        }

        //TextView tv = (TextView)findViewById(R.id.tv_message);
        if ( webServer != null) {
        	currentIpAddress = "http://" + ipAddr  + ":8080";
            //tv.setText( getString(R.string.msg_access_local) + " http://" + ipAddr  + ":8080" );
            return true;
        } else {
            if ( ipAddr == null) {
                //tv.setText( getString(R.string.msg_wifi_error) );
            } else {
                //tv.setText( getString(R.string.msg_port_error) );
            }
            return false;
        }
    }
    private void initCamera() {
        SurfaceView cameraSurface = (SurfaceView)findViewById(R.id.surface_camera);
        cameraView = new CameraView(cameraSurface);
        cameraView.setCameraReadyCallback(this);

        overlayView = (OverlayView)findViewById(R.id.surface_overlay);
    }

    private void resetMediaBuffer() {
        synchronized(MainActivity.this) {
            for (int i = 1; i < MediaBlockNumber; i++) {
                mediaBlocks[i].reset();
            }
            mediaWriteIndex = 0;
            mediaReadIndex = 0;
        }
    }

    private void doStreaming () {
        synchronized(MainActivity.this) {
               
            MediaBlock targetBlock = mediaBlocks[mediaReadIndex];
            if ( targetBlock.flag == 1) {
                streamingServer.sendMedia( targetBlock.data(), targetBlock.length());
                targetBlock.reset();

                mediaReadIndex ++;
                if ( mediaReadIndex >= MediaBlockNumber) {
                    mediaReadIndex = 0;
                } 
            }
        }

        streamingHandler.post(new Runnable() {
            @Override
            public void run() {
                doStreaming();
            }
        });

    }

    protected String wifiIpAddress(Context context) {
        WifiManager wifiManager = (WifiManager) context.getSystemService(WIFI_SERVICE);
        int ipAddress = wifiManager.getConnectionInfo().getIpAddress();

        // Convert little-endian to big-endianif needed
        if (ByteOrder.nativeOrder().equals(ByteOrder.LITTLE_ENDIAN)) {
            ipAddress = Integer.reverseBytes(ipAddress);
        }

        byte[] ipByteArray = BigInteger.valueOf(ipAddress).toByteArray();

        String ipAddressString;
        try {
            ipAddressString = InetAddress.getByAddress(ipByteArray).getHostAddress();
        } catch (UnknownHostException ex) {
            Log.e("WIFIIP", "Unable to get host address.");
            ipAddressString = null;
        }

        return ipAddressString;
    }*/

    //
    //  Internal help class and object definment
    //
    /*private PreviewCallback previewCb = new PreviewCallback() {
        public void onPreviewFrame(byte[] frame, Camera c) {
            previewLock.lock();
            // doVideoEncode(byte[] frame)
            if (inProcessing != true) {
    	        if(cameraView != null) { // HOTFIX!
    	        	inProcessing = true;
    		        int picWidth = cameraView.Width();
    		        int picHeight = cameraView.Height();
    		        int size = picWidth*picHeight + picWidth*picHeight/2;
    		        System.arraycopy(frame, 0, yuvFrame, 0, size);
    		
    		        executor.execute(videoTask);
    	        }
            }
            //
            c.addCallbackBuffer(frame);
            previewLock.unlock();
        }
    };

    private TeaServer.CommonGatewayInterface doQuery = new TeaServer.CommonGatewayInterface () {
        @Override
        public String run(Properties parms) {
            String ret = "";
            ret = "{\"state\": \"ok\",";
            ret = ret + "\"width\": \"" + cameraView.Width() + "\",";
            ret = ret + "\"height\": \"" + cameraView.Height() + "\"}";
            return ret;
        }

        @Override
        public InputStream streaming(Properties parms) {
            return null;
        }
    };

    private class VideoEncodingTask implements Runnable {
        private byte[] resultNal = new byte[1024*1024];
        private byte[] videoHeader = new byte[8];

        public VideoEncodingTask() {
            videoHeader[0] = (byte)0x19;
            videoHeader[1] = (byte)0x79;
        }

        public void run() {
            MediaBlock currentBlock = mediaBlocks[ mediaWriteIndex ];
            if ( currentBlock.flag == 1) {
                inProcessing = false;
                return;
            }

            int intraFlag = 0;
            if ( currentBlock.videoCount == 0) {
                intraFlag = 1;
            }
            int millis = (int)(System.currentTimeMillis() % 65535);
            int ret = nativeDoVideoEncode(yuvFrame, resultNal, intraFlag);
            if ( ret <= 0) {
                return;
            }
            
            // timestamp
            videoHeader[2] = (byte)(millis & 0xFF);
            videoHeader[3] = (byte)((millis>>8) & 0xFF);
            // length
            videoHeader[4] = (byte)(ret & 0xFF);
            videoHeader[5] = (byte)((ret>>8) & 0xFF);
            videoHeader[6] = (byte)((ret>>16) & 0xFF);
            videoHeader[7] = (byte)((ret>>24) & 0xFF);

            synchronized(MainActivity.this) {
                if ( currentBlock.flag == 0) { 
                    boolean changeBlock = false;

                    if ( currentBlock.length() + ret + 8 <= MediaBlockSize ) {
                        currentBlock.write( videoHeader, 8 );
                        currentBlock.writeVideo( resultNal, ret);
                    } else {
                        changeBlock = true;
                    }
                    
                    if ( changeBlock == false ) {
                        if ( currentBlock.videoCount >= EstimatedFrameNumber) {
                            changeBlock = true;
                        }
                    }

                    if ( changeBlock == true) {
                        currentBlock.flag = 1;
                        
                        mediaWriteIndex ++;
                        if ( mediaWriteIndex >= MediaBlockNumber) {
                            mediaWriteIndex = 0;
                        } 
                    }
                }

            }

            inProcessing = false;
        }
    };
    
    private class StreamingServer extends WebSocketServer {
    	private final ConcurrentHashMap<WebSocket, ByteBuffer> mediaSockets = new ConcurrentHashMap<WebSocket, ByteBuffer>();

        public StreamingServer( int port) throws UnknownHostException {
		        super( new InetSocketAddress( port ) );
	      }

        public void sendMedia(byte[] data, int length) {
            if (inStreaming()) {
	            for(ByteBuffer b : mediaSockets.values()) {
	                b.clear();
	                b.put(data, 0, length);
	                b.flip();
	            }
            }

            if (inStreaming()) {
            	for(Entry<WebSocket, ByteBuffer> socket : mediaSockets.entrySet()) {
            		if(socket.getKey().isOpen()) {
            			socket.getKey().send(socket.getValue());
            		}
            	}
            }
        }

        @Override
      	public void onOpen( WebSocket conn, ClientHandshake handshake ) {
            //resetMediaBuffer();
            mediaSockets.put(conn, ByteBuffer.allocate(MediaBlockSize));
            Log.d(TAG, "WebSocket client connected");
      	}

        @Override
	    public void onClose( WebSocket conn, int code, String reason, boolean remote ) {
        	if(mediaSockets.containsKey(conn)) {
        		mediaSockets.remove(conn);
        		Log.d(TAG, "WebSocket client disconnected");
        	}
        }

        @Override
      	public void onError( WebSocket conn, Exception ex ) {
            Log.e(TAG, "Socket error", ex);
      	}

        @Override
      	public void onMessage( WebSocket conn, ByteBuffer blob ) { }

        @Override
        public void onMessage( WebSocket conn, String message ) { }
        
        private boolean inStreaming() {
    		return mediaSockets.size() > 0;				
        }

    }*/

    /*private native void nativeInitMediaEncoder(int width, int height);
    private native void nativeReleaseMediaEncoder(int width, int height);
    private native int nativeDoVideoEncode(byte[] in, byte[] out, int flag);
    private native int nativeDoAudioEncode(byte[] in, int length, byte[] out);*/

    static {
        //System.loadLibrary("MediaEncoder");
    }

	@Override
	public void onLocationChanged(Location location) {
		if(location != null) {
			if(currentLocation != null) {
				if(isBetterLocation(location, currentLocation)) {
					currentLocation = location;
				} else {
					return;
				}
			} else {
				currentLocation = location;
			}
			Log.d(TAG, "onLocationChanged " + location.getLatitude() + " " + location.getLongitude());
			TelemetryRequest request = new TelemetryRequest();
			request.clientId = Config.getClientId(this);
			request.clientType = Config.getClientType(this);
			request.sensorType = "GPS";
			request.data = new Data();
			request.data.position = new Position();
			request.data.position.latitude = currentLocation.getLatitude();
			request.data.position.longitude = currentLocation.getLongitude();
			request.data.speed = location.getSpeed();
			apiService.telemetry(request).enqueue(new Callback<Object>() {
				
				@Override
				public void onResponse(Call<Object> arg0, Response<Object> arg1) {
					if(arg1.isSuccessful()) {
						Log.d(TAG, "GPS coordinates successfully sent");
					} else {
						String message = "";
						try {
							message = new String(arg1.errorBody().bytes());
						} catch (Exception ignore) {}
						Log.e(TAG, "Failed to send gps coordinates. Error message: " + message);
					}
				}
				
				@Override
				public void onFailure(Call<Object> arg0, Throwable arg1) {
					Log.e(TAG, "GPS coordinates request failed", arg1);
				}
			});
		}
	}
	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		Log.d(TAG, "Status of provider has changed " + status);
		
	}
	@Override
	public void onProviderEnabled(String provider) {
		Log.d(TAG, "Provider as been enabled");
	}
	@Override
	public void onProviderDisabled(String provider) {
		Log.d(TAG, "Provider as been disabled");
	}

	private Location currentLocation;
	
	private static final int FIVE_SECONDS = 1000 * 5;

	/** Determines whether one Location reading is better than the current Location fix
	  * @param location  The new Location that you want to evaluate
	  * @param currentBestLocation  The current Location fix, to which you want to compare the new one
	  */
	protected boolean isBetterLocation(Location location, Location currentBestLocation) {
	    if (currentBestLocation == null) {
	        // A new location is always better than no location
	        return true;
	    }

	    // Check whether the new location fix is newer or older
	    long timeDelta = location.getTime() - currentBestLocation.getTime();
	    boolean isSignificantlyNewer = timeDelta > FIVE_SECONDS;
	    boolean isSignificantlyOlder = timeDelta < -FIVE_SECONDS;
	    boolean isNewer = timeDelta > 0;

	    // If it's been more than two minutes since the current location, use the new location
	    // because the user has likely moved
	    if (isSignificantlyNewer) {
	        return true;
	    // If the new location is more than two minutes older, it must be worse
	    } else if (isSignificantlyOlder) {
	        return false;
	    }

	    // Check whether the new location fix is more or less accurate
	    int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
	    boolean isLessAccurate = accuracyDelta > 0;
	    boolean isMoreAccurate = accuracyDelta < 0;
	    boolean isSignificantlyLessAccurate = accuracyDelta > 200;

	    // Check if the old and new location are from the same provider
	    boolean isFromSameProvider = isSameProvider(location.getProvider(),
	            currentBestLocation.getProvider());

	    // Determine location quality using a combination of timeliness and accuracy
	    if (isMoreAccurate) {
	        return true;
	    } else if (isNewer && !isLessAccurate) {
	        return true;
	    } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
	        return true;
	    }
	    return false;
	}

	/** Checks whether two providers are the same */
	private boolean isSameProvider(String provider1, String provider2) {
	    if (provider1 == null) {
	      return provider2 == null;
	    }
	    return provider1.equals(provider2);
	}
	
}
