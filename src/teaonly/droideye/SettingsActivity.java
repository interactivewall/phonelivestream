package teaonly.droideye;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceActivity;

public class SettingsActivity extends PreferenceActivity {

	private static final String TITLE = "title";
	
	public static void start(Context context, String title) {
		Intent i = new Intent(context, SettingsActivity.class);
		i.putExtra(TITLE, title);
		context.startActivity(i);
	}
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
 
        addPreferencesFromResource(R.xml.preferences);
        
        setTitle(getIntent().getStringExtra(TITLE));
 
    }
	
}
